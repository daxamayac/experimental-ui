import { fireEvent, render, screen } from '@testing-library/react';
import { expect, vi } from 'vitest';

import Text, { TextProps } from './index';
import { COLOR_MAP } from './Text.interface';

describe('Text', () => {
  it('Should render Text', () => {
    render(<Text name="render" />);
    expect(screen.getByRole('textbox')).toBeInTheDocument();
  });

  it('Should have input as the only child', () => {
    render(<Text name="render" />);
    expect(screen.getAllByRole('textbox')).toHaveLength(1);
  });

  it('Should call onChange once per change event', () => {
    const mockType = vi.fn();
    render(<Text name="render" onChange={mockType} />);
    fireEvent.change(screen.getByRole('textbox'), {
      target: { value: 'test' },
    });
    expect(mockType).toHaveBeenCalledTimes(1);
  });

  it('Should not call onChange from change event if disabled', () => {
    const mockType = vi.fn();
    render(<Text name="render" onClick={mockType} disabled />);
    fireEvent.change(screen.getByRole('textbox'), {
      target: { value: 'test' },
    });
    expect(mockType).toHaveBeenCalledTimes(0);
  });

  it('Should forward the bordered prop to Input', () => {
    render(<Text name="render" noBordered />);
    expect(screen.getByRole('textbox')).not.toHaveClass('input-bordered');
  });

  it('Should forward the borderOffset prop to Input', () => {
    render(<Text name="render" borderOffset={false} />);
    expect(screen.getByRole('textbox')).toHaveClass('focus:outline-offset-0');
  });

  it('Should forward the placeholder prop to Input', () => {
    render(<Text name="render" placeholder="Write here" />);
    expect(screen.getByRole('textbox')).toHaveAttribute(
      'placeholder',
      'Write here',
    );
  });

  it('Should forward the className prop to Input', () => {
    render(<Text name="render" classes={{ input: 'w-full' }} />);
    expect(screen.getByRole('textbox')).toHaveClass('w-full');
  });

  it.each(Object.entries(COLOR_MAP))(
    'Renders with color "%s"',
    (color, value) => {
      expect(color).toBeDefined();
      expect(value).toBeDefined();
      render(<Text name="name" color={color as TextProps['color']} />);
      expect(screen.getByRole('textbox')).toHaveClass(value);
    },
  );
});
