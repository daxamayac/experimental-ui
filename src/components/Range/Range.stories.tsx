import type { Meta, StoryObj } from '@storybook/react';

import { BADGE } from '@geometricpanda/storybook-addon-badges';

import Range from '.';

const meta = {
  title: 'Inputs/Range',
  component: Range,
  tags: ['autodocs'],
  parameters: {
    badges: [BADGE.EXPERIMENTAL],
  },
  argTypes: {
    startIcon: {
      control: false,
    },
    endIcon: {
      control: false,
    },
  },
} satisfies Meta<typeof Range>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Colors: Story = {
  args: {
    className: 'm-2',
  },

  render: (args) => (
    <div>
      <div>
        <Range {...args} />
      </div>
      <div>
        <Range {...args} color="primary" />
        <Range {...args} color="secondary" />

        <Range {...args} color="accent" />
      </div>
      <div>
        <Range {...args} color="success" />
        <Range {...args} color="info" />
        <Range {...args} color="warning" />
        <Range {...args} color="error" />
      </div>
    </div>
  ),
};
