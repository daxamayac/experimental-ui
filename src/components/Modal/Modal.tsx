import { ReactElement, useRef } from 'react';
import clsx from 'clsx';
import { twMerge } from 'tailwind-merge';

import Button from '../Button';

import { MODAL, MODAL_MAP, ModalProps } from './Modal.interface';
import { useModalEffects } from './useModalEffects';

const Modal = ({
  children,
  open = false,
  onClose,
  hideCloseButton = false,
  disableEscapeKeyDown = false,
  closeOnBackdropClick = false,
  className,
  position = 'responsive',
}: ModalProps): ReactElement => {
  const containerClasses = twMerge(
    MODAL,
    clsx(
      {
        'modal-open': open,
      },
      position !== 'center' && MODAL_MAP[position],
    ),
    className,
  );

  const modalRef = useRef<HTMLDialogElement>(null);

  useModalEffects({
    current: modalRef.current,
    disableEscapeKeyDown,
    onClose,
    open,
  });

  return (
    <dialog className={containerClasses} ref={modalRef}>
      <div className="modal-box">
        {!hideCloseButton && (
          <Button
            size="sm"
            color="ghost"
            shape="circle"
            onClick={onClose}
            className="absolute right-2 top-2 ltr:left-2"
          >
            ✕
          </Button>
        )}
        {children}
      </div>

      {closeOnBackdropClick && (
        <div className="modal-backdrop">
          <button onClick={onClose} className="cursor-default">
            close
          </button>
        </div>
      )}
    </dialog>
  );
};

export default Modal;
