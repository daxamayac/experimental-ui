import { memo, ReactElement } from 'react';
import clsx from 'clsx';

import { LABEL_MAP, LabelProps } from './Label.interface';

const Label = ({
  children,
  label,
  alt,
  error,
  position,
  helper,
  ...props
}: LabelProps): ReactElement => {
  const classesPosition = clsx(position && LABEL_MAP[position]);

  const classesHelper = clsx('label-text-alt', {
    'text-error': !!error,
  });
  return (
    <div>
      <div className={clsx('form-control', classesPosition)}>
        <label {...props} className={'label-text cursor-pointer'}>
          {label}
        </label>

        {!!alt && position === 'top' && (
          <span className="label-text">{alt}</span>
        )}
        {children}
      </div>

      {(helper || error) && (
        <label {...props} className={classesHelper}>
          {error ?? helper}
        </label>
      )}
    </div>
  );
};

export default memo(Label);
