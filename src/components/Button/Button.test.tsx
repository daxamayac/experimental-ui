import { fireEvent, render, screen } from '@testing-library/react';
import { expect } from 'vitest';

import {
  COLOR_MAP,
  SHAPE_MAP,
  SIZE_MAP,
  VARIANT_MAP,
} from './Button.interface';
import Button, { ButtonProps } from './index';

describe('Button', () => {
  it('Should render button', () => {
    render(<Button>test</Button>);
    expect(screen.getByRole('button')).toBeInTheDocument();
  });

  it('Should call onClick once per click event', () => {
    const mockType = vi.fn();
    render(<Button onClick={mockType}>test</Button>);
    fireEvent.click(screen.getByRole('button'));
    expect(mockType).toHaveBeenCalledTimes(1);
  });

  it('Should not call onClick from click event if disabled', () => {
    const mockType = vi.fn();
    render(
      <Button onClick={mockType} disabled>
        test
      </Button>,
    );
    fireEvent.click(screen.getByRole('button'));
    expect(mockType).toHaveBeenCalledTimes(0);
  });

  it('Renders an anchor tag when an href exists', () => {
    render(<Button href="/home">Home</Button>);

    expect(screen.getByRole('link')).toBeTruthy();
    expect(screen.getByRole('link')).toHaveAttribute('href', '/home');
  });

  it('Render with no-animatio', () => {
    render(<Button noAnimation>test</Button>);
    expect(screen.getByRole('button')).toHaveClass('no-animation');
  });

  it('Render with wide', () => {
    render(<Button wide>test</Button>);
    expect(screen.getByRole('button')).toHaveClass('btn-wide');
  });

  it.each(Object.entries(COLOR_MAP))(
    'Renders with color "%s"',
    (color, value) => {
      expect(color).toBeDefined();
      expect(value).toBeDefined();
      const { container } = render(
        <Button color={color as ButtonProps['color']}>Button</Button>,
      );
      expect(container.firstChild).toHaveClass(value);
    },
  );

  it.each(Object.entries(VARIANT_MAP))(
    'Renders with variant "%s"',
    (variant, value) => {
      expect(variant).toBeDefined();
      expect(value).toBeDefined();
      const { container } = render(
        <Button variant={variant as ButtonProps['variant']}>Button</Button>,
      );
      expect(container.firstChild).toHaveClass(value);
    },
  );

  it.each(Object.entries(SIZE_MAP))('Renders with size "%s"', (size, value) => {
    if (size === 'md') {
      return;
    }
    expect(size).toBeDefined();
    expect(value).toBeDefined();
    const { container } = render(
      <Button size={size as ButtonProps['size']}>Button</Button>,
    );
    expect(container.firstChild).toHaveClass(value);
  });

  it.each(Object.entries(SHAPE_MAP))(
    'Renders with shape "%s"',
    (shape, value) => {
      expect(shape).toBeDefined();
      expect(value).toBeDefined();
      const { container } = render(
        <Button shape={shape as ButtonProps['shape']}>Button</Button>,
      );
      expect(container.firstChild).toHaveClass(value);
    },
  );

  it('Render with startIcon', () => {
    render(<Button startIcon={<span>icon-start</span>}>test</Button>);
    const withStartIcon = screen.getByRole('button');
    expect(withStartIcon).toHaveClass('gap-2');
    expect(withStartIcon.firstChild).toContainHTML('<span>icon-start</span>');
  });

  it('Render with endIcon', () => {
    render(<Button endIcon={<span>icon-end</span>}>test</Button>);
    const withEndIcon = screen.getByRole('button');
    expect(withEndIcon).toHaveClass('gap-2');
    expect(withEndIcon.lastChild).toContainHTML('<span>icon-end</span>');
  });

  it('Render with startIcon and endIcon', () => {
    render(
      <Button
        startIcon={<span>icon-start</span>}
        endIcon={<span>icon-end</span>}
      >
        test
      </Button>,
    );
    const withStartIcon = screen.getByRole('button');
    expect(withStartIcon).toHaveClass('gap-2');
    expect(withStartIcon.firstChild).toContainHTML('<span>icon-start</span>');
    expect(withStartIcon.lastChild).toContainHTML('<span>icon-end</span>');
  });

  it('Render with loading', () => {
    render(<Button loading>test</Button>);
    const withLoading = screen.getByRole('button');
    expect(withLoading.firstChild).toContainHTML('<span class="loading"/>');
  });

  it('Render with fullWidth', () => {
    render(<Button fullWidth>test</Button>);
    const withFullWidth = screen.getByRole('button');
    expect(withFullWidth).toHaveClass('btn-block');
  });
});
