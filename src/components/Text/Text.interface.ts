import { InputHTMLAttributes, ReactNode } from 'react';
import {
  FieldError,
  FieldPath,
  FieldValues,
  UseFormRegister,
} from 'react-hook-form';

import { ComponentColor, IComponentBaseProps, Size } from '../types';

export const TEXT = 'input';

export const COLOR_MAP = {
  primary: 'input-primary',
  secondary: 'input-secondary',
  error: 'input-error',
  info: 'input-info',
  success: 'input-success',
  warning: 'input-warning',
  accent: 'input-accent',
  ghost: 'input-ghost',
};

export const SIZE_MAP = {
  xs: 'input-xs',
  sm: 'input-sm',
  md: 'input-md',
  lg: 'input-lg',
};

export const TEXT_MAP = {
  ...SIZE_MAP,
  ...COLOR_MAP,
};

export interface TextProps
  extends Omit<InputHTMLAttributes<HTMLInputElement>, 'size' | 'color' | 'id'>,
    IComponentBaseProps {
  noBordered?: boolean;
  borderOffset?: boolean;
  size?: Size;
  placeholder?: string;
  name: FieldPath<FieldValues>;
  color?: Exclude<ComponentColor, 'neutral'>;
  register?: UseFormRegister<FieldValues>;
  label?: ReactNode;
  classes?: {
    input?: string;
    label?: string;
    labelText?: string;
    helper?: string;
    helperText?: string;
  };
  error?: FieldError;
  helper?: string;
}
