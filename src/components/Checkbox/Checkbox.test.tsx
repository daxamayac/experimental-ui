import { render, screen } from '@testing-library/react';
import { useState } from 'react';
import { expect } from 'vitest';

import userEvent from '@testing-library/user-event';

import { COLOR_MAP } from './Checkbox.interface';
import Checkbox, { CheckboxProps } from './index';

const ControlledCheckbox = () => {
  const [checked, setChecked] = useState(false);

  return (
    <Checkbox
      name="checkbox-name"
      checked={checked}
      onChange={({ target: { checked } }) => setChecked(checked)}
    />
  );
};

describe('Checkbox', () => {
  it('Should render', () => {
    render(<Checkbox name="checkbox-name" />);
    expect(screen.getByRole('checkbox')).toBeInTheDocument();
  });

  it('Should not be checked by default', () => {
    render(<Checkbox name="checkbox-name" />);
    expect(screen.getByRole('checkbox')).not.toBeChecked();
  });

  it('Should get checked on click', async () => {
    render(<Checkbox name="checkbox-name" />);
    await userEvent.click(screen.getByRole('checkbox'));
    expect(screen.getByRole('checkbox')).toBeChecked();
  });

  it('Should get checked on click - controlled component', async () => {
    render(<ControlledCheckbox />);
    await userEvent.click(screen.getByRole('checkbox'));
    expect(screen.getByRole('checkbox')).toBeChecked();
  });

  it('Should not get checked on click if disabled', async () => {
    render(<Checkbox name="checkbox-name" disabled />);
    await userEvent.click(screen.getByRole('checkbox'));
    expect(screen.getByRole('checkbox')).not.toBeChecked();
  });

  it('Should be checked if checked prop is true', () => {
    render(<Checkbox name="checkbox-name" checked readOnly />);
    expect(screen.getByRole('checkbox')).toBeChecked();
  });

  it('Should be checked if defaultChecked prop is true', () => {
    render(<Checkbox name="checkbox-name" defaultChecked />);
    expect(screen.getByRole('checkbox')).toBeChecked();
  });

  it('Should get focused on tab key press', async () => {
    render(<Checkbox name="checkbox-name" />);
    await userEvent.tab();
    expect(screen.getByRole('checkbox')).toHaveFocus();
  });

  it('(focused) Should get checked on space key press', async () => {
    render(<Checkbox name="checkbox-name" />);
    await userEvent.tab();
    await userEvent.keyboard('[Space]');
    expect(screen.getByRole('checkbox')).toBeChecked();
  });

  it('Should be indeterminate if indeterminate prop is true', () => {
    render(<Checkbox name="checkbox-name" indeterminate />);
    const checkbox: HTMLInputElement = screen.getByRole('checkbox');
    expect(checkbox).toBeInstanceOf(HTMLInputElement);
    expect(checkbox.indeterminate).toBe(true);
  });

  it.each(Object.entries(COLOR_MAP))(
    'Renders with color "%s"',
    (color, value) => {
      expect(color).toBeDefined();
      expect(value).toBeDefined();
      render(
        <Checkbox
          name="checkbox-name"
          color={color as CheckboxProps['color']}
        />,
      );
      expect(screen.getByRole('checkbox')).toHaveClass(value);
    },
  );

  it('Should render class based on className prop', () => {
    const testClass = '123asd123asd';
    render(<Checkbox name="checkbox-name" className={testClass} />);
    expect(screen.getByRole('checkbox')).toHaveClass(testClass);
  });
});
