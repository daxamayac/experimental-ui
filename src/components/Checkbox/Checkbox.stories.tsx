import type { Meta, StoryObj } from '@storybook/react';

import { BADGE } from '@geometricpanda/storybook-addon-badges';

import Checkbox from '.';
import { v4 as uuid } from 'uuid';

const meta = {
  title: 'Inputs/Checkbox',
  component: Checkbox,
  tags: ['autodocs'],
  parameters: {
    badges: [BADGE.EXPERIMENTAL],
  },
  args: {
    name: uuid(),
    label: 'Default Label',
  },
} satisfies Meta<typeof Checkbox>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Colors: Story = {
  render: (args) => (
    <div className="flex flex-col gap-y-2">
      <Checkbox {...args} />
      <Checkbox {...args} color="primary" />
      <Checkbox {...args} color="secondary" />
      <Checkbox {...args} color="accent" />
    </div>
  ),
};
export const ColorsWithLabel: Story = {
  render: (args) => (
    <div>
      <div className="flex flex-col gap-2 bg-gradient-to-r from-blue-800 to-cyan-500 p-2">
        <Checkbox {...args} />
        <Checkbox {...args} label="No border" />
        <Checkbox {...args} label="Ghost" />
      </div>
      <div className="flex flex-col gap-2 p-2">
        <Checkbox {...args} label="Default" />
        <Checkbox {...args} color="primary" label="Primary" />
        <Checkbox {...args} color="secondary" label="Secondary" />
        <Checkbox {...args} color="accent" label="Accent" />
        <Checkbox {...args} label="Ghost" />
      </div>
      <div className="flex flex-col gap-2 p-2">
        <Checkbox {...args} color="success" label="success" />
        <Checkbox {...args} color="info" label="info" />
        <Checkbox {...args} color="warning" label="warning" />
        <Checkbox {...args} color="error" label="Error" />
      </div>
    </div>
  ),
};

export const SizeWithLabel: Story = {
  render: (args) => (
    <div className="flex flex-col gap-2 p-2">
      <div className="flex flex-col gap-2 p-2">
        <Checkbox {...args} label="Default" />
      </div>

      <div className="flex flex-col gap-2 p-2">
        <Checkbox {...args} size="xs" label="Size xs" />
        <Checkbox {...args} size="sm" label="Size sm" />
        <Checkbox {...args} size="md" label="Size md" />
        <Checkbox {...args} size="lg" label="Size lg" />
      </div>
    </div>
  ),
};
