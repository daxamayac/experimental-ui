import type { Meta } from '@storybook/react';

import { useState } from 'react';

import { Button, ModalProps } from '../index';
import { BADGE } from '@geometricpanda/storybook-addon-badges';

import Modal from '.';
const meta = {
  title: 'Data Display/Modal',
  component: Modal,
  tags: ['autodocs'],
  parameters: {
    badges: [BADGE.EXPERIMENTAL],
  },
} satisfies Meta<typeof Modal>;

export default meta;
export const Default = (args: ModalProps) => {
  const [modalOpen, setModalOpen] = useState(false);
  const handleClose = () => {
    setModalOpen(false);
  };

  const handleAgree = () => {
    setModalOpen(false);
  };

  const argsModal = {
    ...args,
    onClose: handleClose,
    open: modalOpen,
  };

  return (
    <div className="flex flex-col gap-2">
      <Modal {...argsModal}>
        Modal info: form or message
        <div className="modal-action">
          <Button onClick={handleClose}>Close</Button>
          <Button onClick={handleAgree}>Agree</Button>
        </div>
      </Modal>
      <Button onClick={() => setModalOpen(true)}>Open Modal</Button>
    </div>
  );
};
