import { forwardRef, memo, ReactElement } from 'react';
import { mergeRefs } from 'react-merge-refs';
import clsx from 'clsx';
import { twMerge } from 'tailwind-merge';
import {
  FieldError,
  RefCallBack,
  useFormContext,
  UseFormRegisterReturn,
  UseFormReturn,
} from 'react-hook-form';

import Label from '../Label';

import { TEXT, TEXT_MAP, TextProps } from './Text.interface';

const Text = forwardRef<HTMLInputElement, TextProps>(
  (
    {
      name,
      noBordered = false,
      borderOffset = true,
      size,
      color,
      dataTheme,
      classes,
      register,
      error,
      helper,
      label,
      disabled,
      ...props
    },
    ref,
  ): ReactElement => {
    const formContext: UseFormReturn = useFormContext();
    let registerRef: RefCallBack;
    let registerRest: Omit<UseFormRegisterReturn, 'ref'> | undefined;

    if (formContext) {
      register = formContext.register;
      error = error ?? (formContext.formState.errors[name] as FieldError);
      disabled = disabled ?? formContext.formState.isSubmitting;
      const { ref, ...rest } = register(name);
      registerRest = rest;
      registerRef = ref;
    }

    const classesInput = twMerge(
      TEXT,
      classes?.input,
      clsx(size && TEXT_MAP[size], color && TEXT_MAP[color], {
        'input-error': !!error,
        'input-bordered': !noBordered,
        'focus:outline-offset-0': !borderOffset,
      }),
    );

    const textInput = (
      <input
        type="text"
        ref={(el: HTMLInputElement) => {
          mergeRefs([registerRef, ref])(el);
        }}
        id={name}
        name={name}
        disabled={disabled}
        className={classesInput}
        data-theme={dataTheme}
        {...props}
        {...registerRest}
      />
    );

    if (label) {
      return (
        <Label
          htmlFor={name}
          position="top"
          label={label}
          helper={helper}
          error={error?.message}
        >
          {textInput}
        </Label>
      );
    }
    return textInput;
  },
);
Text.displayName = 'Text';
export default memo(Text);
