import { HTMLAttributes } from 'react';

import { IComponentBaseProps } from '../types';

export const MODAL = 'modal';

export const MODAL_MAP = {
  bottom: 'modal-bottom',
  center: 'modal-middle',
  top: 'modal-top',
  responsive: 'modal-bottom sm:modal-middle',
};

export interface ModalProps
  extends HTMLAttributes<HTMLDialogElement>,
    IComponentBaseProps {
  open?: boolean;
  onClose: () => void;
  hideCloseButton?: boolean;
  disableEscapeKeyDown?: boolean;
  closeOnBackdropClick?: boolean;
  position?: keyof typeof MODAL_MAP;
}
