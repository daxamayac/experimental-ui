import { forwardRef, ReactElement } from 'react';
import clsx from 'clsx';
import { twMerge } from 'tailwind-merge';

import { defaultSize } from '../constants';
import Loading from '../Loading';

import { BTN, BUTTON_MAP, ButtonProps } from './Button.interface';

const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  (
    {
      children,
      href,
      shape,
      size = defaultSize,
      variant,
      color,
      startIcon,
      endIcon,
      wide,
      fullWidth,
      responsive,
      noAnimation,
      loading,
      loadingVariant,
      active,
      disabled,
      dataTheme,
      className,
      style,
      ...props
    },
    ref,
  ): ReactElement => {
    const classNameButton = twMerge(
      BTN,
      clsx(
        ((startIcon && !loading) || endIcon) && 'gap-2',
        color && BUTTON_MAP[color],
        variant && BUTTON_MAP[variant],
        size !== defaultSize && BUTTON_MAP[size],
        shape && BUTTON_MAP[shape],
        wide && BUTTON_MAP['wide'],
        fullWidth && BUTTON_MAP['fullWidth'],
        responsive && BUTTON_MAP['responsive'],
        active && BUTTON_MAP['active'],
        disabled && BUTTON_MAP['disabled'],
        noAnimation && BUTTON_MAP['noAnimation'],
        className,
      ),
    );

    if (href) {
      return (
        <a className={classNameButton} style={style} href={href}>
          {!!startIcon && startIcon}
          {children}
          {!!endIcon && endIcon}
        </a>
      );
    }
    return (
      <button
        {...props}
        ref={ref}
        data-theme={dataTheme}
        className={classNameButton}
        style={style}
        disabled={disabled ?? loading}
      >
        {loading && <Loading variant={loadingVariant} size={size} />}
        {!!startIcon && startIcon}
        {children}
        {!!endIcon && endIcon}
      </button>
    );
  },
);
Button.displayName = 'Button';

export default Button;
