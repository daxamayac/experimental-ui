import react from '@vitejs/plugin-react-swc';

import * as packageJson from './package.json';

import { resolve } from 'node:path';
import { defineConfig } from 'vite';
import dts from 'vite-plugin-dts';
import EsLint from 'vite-plugin-linter';
import tsConfigPaths from 'vite-tsconfig-paths';

const { EsLinter, linterPlugin } = EsLint;

// https://vitejs.dev/config/

export default defineConfig((configEnv) => ({
  plugins: [
    react(),
    tsConfigPaths(),
    linterPlugin({
      include: ['./src}/**/*.{ts,tsx}'],
      linters: [new EsLinter({ configEnv })],
    }),
    dts(),
  ],
  server: {
    open: true,
  },
  test: {
    globals: true,
    environment: 'jsdom',
    setupFiles: ['./test.setup.ts'],
    coverage: {
      all: true,
      provider: 'v8',
      include: ['src/**/*.{ts,tsx}'],
      exclude: [
        'src/**/*.stories.tsx',
        'src/App.tsx',
        'src/index.ts',
        'src/main.tsx',
        'src/vite-env.d.ts',
      ],
      reporter: ['html', 'lcov'],
    },
  },
  build: {
    lib: {
      entry: {
        index: resolve('src', 'index.ts'),
        'components/index': resolve('src', 'components/index.ts'),
        'components/Button/index': resolve('src', 'components/Button/index.ts'),
        'components/Checkbox/index': resolve(
          'src',
          'components/Checkbox/index.ts',
        ),
        'components/Form/index': resolve('src', 'components/Form/index.ts'),
        'components/Loading/index': resolve(
          'src',
          'components/Loading/index.ts',
        ),
        'components/Modal/index': resolve('src', 'components/Modal/index.ts'),
        'components/Range/index': resolve('src', 'components/Range/index.ts'),
        'components/Swap/index': resolve('src', 'components/Swap/index.ts'),
        'components/Text/index': resolve('src', 'components/Text/index.ts'),
        'components/Toggle/index': resolve('src', 'components/Toggle/index.ts'),
      },
      formats: ['es'],
      emptyOutDir: false,
      sourcemap: false,
    },
    rollupOptions: {
      external: [...Object.keys(packageJson.peerDependencies)],
    },
  },
}));
