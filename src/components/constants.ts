export const positions = ['top', 'right', 'bottom', 'left'] as const;
export const shapes = ['circle', 'square'] as const;
export const sizes = ['xs', 'sm', 'md', 'lg'] as const;
export const statuses = ['success', 'info', 'warning', 'error'] as const;
export const colors = ['primary', 'secondary', 'accent', 'neutral'] as const;
export const componentsColors = [...colors, 'ghost', ...statuses] as const;
export const defaultTheme = 'light';

export const GAP_MAP = {
  xs: 'dax-gap-xs',
  sm: 'dax-gap-sm',
  md: 'dax-gap-md',
  lg: 'dax-gap-lg',
};

export const defaultSize = 'md';
