import { render } from '@testing-library/react';
import { expect } from 'vitest';

import Rating from './Rating';

describe('Rating', () => {
  it('Should render Rating', () => {
    const { container } = render(<Rating />);
    expect(container.querySelector('.rating')).toBeInTheDocument();
  });
});
