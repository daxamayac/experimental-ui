import Swap from './Swap';

export type { SwapProps } from './Swap.interface';
export default Swap;
