import { memo, ReactElement, useEffect, useRef } from 'react';
import clsx from 'clsx';
import { twMerge } from 'tailwind-merge';
import {
  FieldError,
  RefCallBack,
  useFormContext,
  UseFormRegisterReturn,
} from 'react-hook-form';

import { defaultSize } from '../constants';
import Label from '../Label';

import { CHECKBOX, CHECKBOX_MAP, CheckboxProps } from './Checkbox.interface';

const Checkbox = ({
  name,
  color,
  label,
  labelPosition = 'right',
  size = defaultSize,
  dataTheme,
  indeterminate = false,
  register,
  disabled,
  error,
  ...props
}: CheckboxProps): ReactElement => {
  const checkboxRef = useRef<HTMLInputElement>();
  const formContext = useFormContext();

  let restRegister: Omit<UseFormRegisterReturn, 'ref'> | undefined;
  let refRegister: RefCallBack;

  if (formContext) {
    const { register: registerFormContext, formState } = formContext;
    const defaultValue = formState.defaultValues?.[name] ?? null;
    const indeterminateFromValue = defaultValue === null;
    const isSubmitting = formState.isSubmitting;
    error = error ?? (formState.errors[name] as FieldError);
    register = registerFormContext;
    disabled = disabled ?? isSubmitting;
    indeterminate = indeterminateFromValue;
    const { ref, ...rest } = register(name, {
      value: indeterminateFromValue ? null : false,
    });

    restRegister = rest;
    refRegister = ref;
  }

  useEffect(() => {
    if (checkboxRef.current) {
      checkboxRef.current.indeterminate = indeterminate || false;
    }
  }, [indeterminate]);

  const clasessCheckbox = twMerge(
    CHECKBOX,
    clsx(
      size != defaultSize && CHECKBOX_MAP[size],
      color && CHECKBOX_MAP[color],
      {
        'checkbox-error': !!error,
      },
    ),
  );

  const checkboxInput = (
    <input
      type="checkbox"
      id={name}
      ref={(el: HTMLInputElement) => {
        checkboxRef.current = el;
        if (refRegister) {
          refRegister(el);
        }
      }}
      data-theme={dataTheme}
      className={clasessCheckbox}
      disabled={disabled}
      {...(restRegister ?? {})}
      {...props}
    />
  );
  if (label) {
    return (
      <Label
        htmlFor={name}
        position={labelPosition}
        label={label}
        error={error?.message}
      >
        {checkboxInput}
      </Label>
    );
  }

  return checkboxInput;
};

export default memo(Checkbox);
