import type { Meta, StoryObj } from '@storybook/react';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { BADGE } from '@geometricpanda/storybook-addon-badges';

import Button from '.';
const meta = {
  title: 'Inputs/Button',
  component: Button,
  tags: ['autodocs'],
  parameters: {
    badges: [BADGE.BETA],
  },
  argTypes: {
    startIcon: {
      control: false,
    },
    endIcon: {
      control: false,
    },
  },
} satisfies Meta<typeof Button>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Colors: Story = {
  render: (args) => (
    <div className="flex flex-col gap-2">
      <div>
        <Button {...args}>Default</Button>
      </div>
      <div className="flex gap-2">
        <Button {...args} color="primary">
          Primary
        </Button>
        <Button {...args} color="secondary">
          Default
        </Button>

        <Button {...args} color="accent">
          Accent
        </Button>
        <Button {...args} color="ghost">
          Ghost
        </Button>
      </div>
      <div className="flex gap-2">
        <Button {...args} color="success">
          Success
        </Button>
        <Button {...args} color="info">
          Info
        </Button>
        <Button {...args} color="warning">
          Warning
        </Button>
        <Button {...args} color="error">
          Error
        </Button>
      </div>
    </div>
  ),
};

export const Size: Story = {
  render: (args) => (
    <div className="flex flex-col gap-2">
      <div>
        <Button {...args}>Default</Button>
      </div>

      <div className="flex items-center gap-2">
        <Button {...args} size="xs">
          Button xs
        </Button>
        <Button {...args} size="sm">
          Button sm
        </Button>
        <Button {...args} size="md">
          Button md
        </Button>
        <Button {...args} size="lg">
          Button lg
        </Button>
      </div>
    </div>
  ),
};

export const Shape: Story = {
  render: (args) => (
    <div className="flex flex-col gap-2">
      <div>
        <Button {...args}>Default</Button>
      </div>
      <div className="flex gap-2">
        <Button {...args} shape="square">
          Square
        </Button>
        <Button {...args} shape="circle">
          Rounded
        </Button>
      </div>
    </div>
  ),
};

export const Variants: Story = {
  render: (args) => (
    <div className="flex flex-col gap-2">
      <div>
        <Button {...args}>Default</Button>
      </div>

      <div className="bg-gradient-to-r from-cyan-500 to-blue-500">
        <Button {...args} variant="outline">
          Outline
        </Button>
        <Button {...args} variant="link">
          Link
        </Button>
        <Button {...args} variant="glass">
          Glass
        </Button>
      </div>
    </div>
  ),
};

const StartIcon = <FontAwesomeIcon icon={faWhatsapp} />;
const EndIcon = <FontAwesomeIcon icon={faWhatsapp} />;

export const Icons: Story = {
  render: (args) => (
    <div className="flex gap-2">
      <Button {...args} startIcon={StartIcon}>
        Start Icon
      </Button>
      <Button {...args} endIcon={EndIcon}>
        Start Icon
      </Button>
      <Button {...args} startIcon={StartIcon} endIcon={EndIcon}>
        Start & End
      </Button>
      <Button {...args} endIcon={EndIcon}>
        End Icon
      </Button>
    </div>
  ),
};

export const Href: Story = {
  render: (args) => (
    <div className="flex gap-2">
      <Button {...args} href="https://google.com" rel="noopener noreferrer">
        To Google
      </Button>
    </div>
  ),
};
