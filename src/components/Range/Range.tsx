import { memo, ReactElement, useMemo } from 'react';
import clsx from 'clsx';
import { twMerge } from 'tailwind-merge';
import { useFormContext } from 'react-hook-form';

import { defaultSize } from '../constants';

import CustomMarks from './Range.CustomMarks';
import DefaultMarks from './Range.DefaultMarks';
import { PADDING_X_MAP, RANGE, RANGE_MAP, RangeProps } from './Range.interface';

//TODO move thumb to mark after click && add label

/**
 * @status: development
 * @important: no production
 *
 */
const Range = ({
  name,
  register,
  color,
  size,
  step,
  min = 0,
  max = 100,
  marks,
  dataTheme,
  className,
  classes,
  disabled,
  ...props
}: RangeProps): ReactElement => {
  const formContext = useFormContext();

  if (formContext) {
    register = formContext.register;
    // TODO errors = formContext.formState.errors[name];
    //   disabled = disabled || formContext.formState.isSubmitting;
  }

  const rangeClassName = twMerge(
    RANGE,
    'block',
    clsx(size && RANGE_MAP[size], color && RANGE_MAP[color]),
    className,
  );

  const paddingFixMarks = twMerge(PADDING_X_MAP[size ?? defaultSize]);

  const marksClassName = useMemo(
    () => clsx(paddingFixMarks, classes?.customMarks?.container),
    [paddingFixMarks, classes],
  );

  const marksClasses = useMemo(
    () => ({
      mark: clsx(classes?.customMarks?.mark),
    }),
    [classes],
  );

  return (
    <div className="w-full">
      <input
        type="range"
        step={step}
        min={min}
        max={max}
        disabled={disabled}
        data-theme={dataTheme}
        className={rangeClassName}
        {...props}
        {...register?.(name)}
      />

      {!!step && (
        <DefaultMarks
          step={step}
          min={min}
          max={max}
          className={marksClassName}
          classes={marksClasses}
        />
      )}
      {!!marks && (
        <CustomMarks
          min={min}
          max={max}
          marks={marks}
          className={marksClassName}
          classes={marksClasses}
        />
      )}
    </div>
  );
};

export default memo(Range);
