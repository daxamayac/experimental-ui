import Loading from './Loading';

export type { LoadingProps } from './Loading.interface';
export default Loading;
