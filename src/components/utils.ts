import {
  cloneElement,
  Fragment,
  isValidElement,
  ReactElement,
  ReactNode,
} from 'react';
import { twMerge } from 'tailwind-merge';

// Returns true if an element is a react fragment
export const isReactFragment = (node: ReactNode | typeof Fragment) => {
  if (!node) return false;

  if ((node as ReactElement)?.type) {
    return (node as ReactElement)?.type === Fragment;
  }

  return node === Fragment;
};

// If an invalid element or fragment is passed in as the node, wrap it with the wrapper and add props
// If a valid element is passed, add the props
export const wrapWithElementIfInvalid = ({
  node,
  wrapper,
  props,
}: {
  node: ReactNode;
  wrapper: ReactElement;
  props: { className: string };
}): ReactElement => {
  if (!node) {
    return cloneElement(wrapper, props);
  } else if (!isValidElement(node)) {
    return cloneElement(wrapper, props, node);
  } else if (isReactFragment(node)) {
    return cloneElement(
      wrapper,
      { ...props, className: twMerge(node.props?.className, props?.className) },
      node.props.children,
    );
  } else {
    const { className, ...rest } = props ?? {};
    const mergedClassName = twMerge(node.props?.className, className);
    return cloneElement(
      node,
      Object.assign({}, rest, { className: mergedClassName }),
    );
  }
};
