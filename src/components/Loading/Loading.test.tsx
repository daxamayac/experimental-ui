import { render } from '@testing-library/react';
import { expect } from 'vitest';

import Loading, { LoadingProps } from './index';
import { COLOR_MAP, SIZE_MAP, VARIANT_MAP } from './Loading.interface';

describe('Loading', () => {
  it('Should render default Loading', () => {
    const { container } = render(<Loading />);
    expect(container.querySelector('.loading')).toHaveClass('loading');
  });

  it.each(Object.entries(SIZE_MAP))(
    'Should render Loading with size "%s"',
    (size, value) => {
      if (size === 'md') {
        return;
      }
      expect(size).toBeDefined();
      expect(value).toBeDefined();
      const { container } = render(
        <Loading size={size as LoadingProps['size']} />,
      );
      expect(container.firstChild).toHaveClass(value);
    },
  );

  it.each(Object.entries(COLOR_MAP))(
    'Should render Loading with color "%s"',
    (color, value) => {
      expect(color).toBeDefined();
      expect(value).toBeDefined();
      const { container } = render(
        <Loading color={color as LoadingProps['color']} />,
      );
      expect(container.firstChild).toHaveClass(value);
    },
  );

  it.each(Object.entries(VARIANT_MAP))(
    'Should render Loading with variant "%s"',
    (variant, value) => {
      if (variant === 'spinner') {
        return;
      }
      expect(variant).toBeDefined();
      expect(value).toBeDefined();
      const { container } = render(
        <Loading variant={variant as LoadingProps['variant']} />,
      );
      expect(container.firstChild).toHaveClass(value);
    },
  );
});
