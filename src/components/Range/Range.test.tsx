import { render, screen } from '@testing-library/react';

import Range from './index';

describe('Range', () => {
  it('Should not render with invalid step', () => {
    render(<Range name="range-name" />);
    expect(screen.queryByText('|')).not.toBeInTheDocument();
  });

  it('Should render Range', () => {
    render(<Range name="range-name" data-testid="range" step={25} />);
    expect(screen.getByTestId('range')).toBeInTheDocument();
    expect(screen.getAllByText('|')).toHaveLength(5);
  });

  it('Should render Range with 40 step', () => {
    expect(() => render(<Range name="step-error" step={40} />)).toThrowError(
      'step no es múltiplo de la diferencia entre min y max',
    );
  });
});
