import Checkbox from './Checkbox';

export type { CheckboxProps } from './Checkbox.interface';
export default Checkbox;
