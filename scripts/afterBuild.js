import fs from 'fs';

const files = fs.readdirSync('./dist');
files.forEach((file) => {
  if (file.endsWith('.js')) {
    const source = fs.readFileSync(`./dist/${file}`, 'utf8');
    const lines = source.split('\n');
    lines.splice(0, 0, '"use client"');
    fs.writeFileSync(`./dist/${file}`, lines.join('\n'));
  }
});
