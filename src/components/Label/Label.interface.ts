import { LabelHTMLAttributes, ReactNode } from 'react';

import { IComponentBaseProps } from '../types';

type Position = 'left' | 'right' | 'top';
export const POSITION_MAP: Record<Position, string> = {
  left: 'flex-row ',
  right: 'flex-row-reverse',
  top: 'items-start',
};

export const LABEL_MAP = {
  ...POSITION_MAP,
};
export interface LabelProps
  extends Omit<LabelHTMLAttributes<HTMLLabelElement>, 'label'>,
    IComponentBaseProps {
  label: ReactNode;
  alt?: string;
  helper?: string;
  error?: string;
  position?: Position;
}
