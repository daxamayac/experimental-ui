import { render, screen } from '@testing-library/react';

import Toggle from './index';
import { COLOR_MAP, ToggleProps } from './Toggle.interface.ts';

describe('Toggle', () => {
  it('should render', () => {
    render(<Toggle name="tooggle-name" data-testid="toggler" />);
    expect(screen.getByTestId('toggler')).toBeInTheDocument();
  });

  it('should render disabled', () => {
    render(<Toggle name="tooggle-name" disabled data-testid="toggler" />);
    expect(screen.getByTestId('toggler')).toBeDisabled();
  });

  it('should render checked', () => {
    render(<Toggle name="tooggle-name" defaultChecked data-testid="toggler" />);
    expect(screen.getByTestId('toggler')).toBeChecked();
  });

  it('should render unchecked', () => {
    render(<Toggle name="tooggle-name" data-testid="toggler" />);
    expect(screen.getByTestId('toggler')).not.toBeChecked();
  });

  it('should render disabled and checked', () => {
    render(
      <Toggle
        name="tooggle-name"
        defaultChecked
        disabled
        data-testid="toggler"
      />,
    );
    expect(screen.getByTestId('toggler')).toBeDisabled();
    expect(screen.getByTestId('toggler')).toBeChecked();
  });

  it.each(Object.entries(COLOR_MAP))(
    'Renders with color "%s"',
    (color, value) => {
      expect(color).toBeDefined();
      expect(value).toBeDefined();
      render(
        <Toggle
          name="name"
          data-testid="toggle-colors"
          color={color as ToggleProps['color']}
        />,
      );
      expect(screen.getByTestId('toggle-colors')).toHaveClass(value);
    },
  );
});
