import Toggle from './Toggle';

export type { ToggleProps } from './Toggle.interface';
export default Toggle;
