import { InputHTMLAttributes } from 'react';
import { FieldValues, UseFormRegister } from 'react-hook-form';

import { ComponentColor, IComponentBaseProps, Size } from '../types';

import { MarkProps } from './Range.CustomMarks';

export const RANGE = 'range';

const SIZE_MAP = {
  xs: 'range-xs',
  sm: 'range-sm',
  md: 'range-md',
  lg: 'range-lg',
};

export const PADDING_X_MAP = {
  xs: 'range-padding-x-xs',
  sm: 'range-padding-x-sm',
  md: 'range-padding-x-md',
  lg: 'range-padding-x-lg',
};

const COLOR_MAP = {
  primary: 'range-primary',
  secondary: 'range-secondary',
  error: 'range-error',
  info: 'range-info',
  success: 'range-success',
  warning: 'range-warning',
  accent: 'range-accent',
};

export const RANGE_MAP = {
  ...SIZE_MAP,
  ...COLOR_MAP,
  vertical: 'range-vertical',
};

export type RangeProps = Omit<InputHTMLAttributes<HTMLInputElement>, 'size'> &
  IComponentBaseProps & {
    color?: Exclude<ComponentColor, 'ghost' | 'neutral'>;
    size?: Size;
    step?: number;
    min?: number;
    max?: number;
    name: string;
    register?: UseFormRegister<FieldValues>;
    // vertical?: boolean;
    // reverse?: boolean;
    hiddeStepValue?: boolean;
    marks?: MarkProps[];
    classes?: {
      customMarks?: {
        container?: string;
        mark?: string;
      };
      defaultMarks?: {
        container?: string;
        mark?: string;
      };
    };
  };
