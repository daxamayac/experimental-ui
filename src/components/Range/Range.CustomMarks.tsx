import { memo, ReactNode } from 'react';
import clsx from 'clsx';
import { twMerge } from 'tailwind-merge';

type ShowInScreen = 'none' | 'all' | 'sm' | 'md' | 'lg' | 'xl';

const RANGE_VISIBLE_MAP = {
  none: 'invisible',
  all: 'visible',
  sm: 'sm:visible',
  md: 'md:visible',
  lg: 'lg:visible',
  xl: 'xl:visible',
};

// Mobile first, then by default first and last are visible
// the rest are invisible
// to make them visible, add prop showInScreen with the screens you want to show
export interface MarkProps {
  value: number;
  label: ReactNode;
  showInScreen?: ShowInScreen;
}

function CustomMarks({
  marks,
  max,
  classes,
  className,
  min,
}: {
  marks: MarkProps[];
  className?: string;
  classes?: { mark?: string };
  max: number;
  min: number;
}) {
  const sortedMarks = marks.slice().sort((a, b) => a.value - b.value);

  return (
    <div className={clsx(' w-full', className)}>
      <div className="relative flex w-full ">
        {sortedMarks.map((mark) => {
          const distance = (mark.value - min) / (max - min);
          const left = `${distance * 100}%`;

          return (
            <div
              className={twMerge(
                'absolute -translate-x-1/2 cursor-pointer text-center first:relative',
                'first:visible last:visible invisible ',
                mark?.showInScreen && RANGE_VISIBLE_MAP[mark.showInScreen],
                classes?.mark,
              )}
              style={{ left: left }}
              key={mark.value}
            >
              {mark.label}
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default memo(CustomMarks);
