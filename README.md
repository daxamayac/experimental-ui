# `Experimental` ui
# ===================
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=daxamayac_experimental-ui&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=daxamayac_experimental-ui)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=daxamayac_experimental-ui&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=daxamayac_experimental-ui)

### Check out our Storybook [here](https://daxamayac.gitlab.io/experimental-ui/)
# ===================
Is `experimental` UI library that integrate the commons libraries used un React App
The following libraries are integrated:
- [x] [DaisyUI] (https://daisyui.com/)
- [x] [React Hook Form] (https://react-hook-form.com/)


Inspired by [react-daisyui] (https://github.com/daisyui/react-daisyui)


This its a simple way to create forms with validations based on [React Hook Form] (https://react-hook-form.com/), and the styles are based on [DaisyUI] (https://daisyui.com/)

