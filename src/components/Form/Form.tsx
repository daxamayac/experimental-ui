import { twMerge } from 'tailwind-merge';
import { FieldValues, FormProvider, useForm } from 'react-hook-form';

import { FormProps } from './Form.interface';

// TODO: Add support for useForm as parameter
function Form<TFieldValues extends FieldValues>({
  mode,
  defaultValues,
  children,
  className,
  onSubmit,
  resolver,
}: Readonly<FormProps<TFieldValues>>) {
  const methods = useForm<TFieldValues>({
    mode,
    defaultValues,
    resolver,
  });

  return (
    <FormProvider {...methods}>
      <form
        onSubmit={methods.handleSubmit(onSubmit)}
        className={twMerge(className)}
      >
        {children}
      </form>
    </FormProvider>
  );
}

export default Form;
