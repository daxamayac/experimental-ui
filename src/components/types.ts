import { colors, componentsColors, shapes, sizes, statuses } from './constants';
import { defaultThemes } from './defaultThemes';

export type DataTheme = (typeof defaultThemes)[number] | string;
export interface IComponentBaseProps {
  dataTheme?: DataTheme;
}

export type Color = (typeof colors)[number];
export type Shape = (typeof shapes)[number];
export type Size = (typeof sizes)[number];
export type Status = (typeof statuses)[number];
export type ComponentColor = (typeof componentsColors)[number];

//TODO bgColors
