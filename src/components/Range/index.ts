import Range from './Range';

export type { RangeProps } from './Range.interface';
export default Range;
