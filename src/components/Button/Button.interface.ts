import { ButtonHTMLAttributes, ReactNode } from 'react';

import { Variant as LoadingVariant } from '../Loading/Loading.interface';
import { ComponentColor, IComponentBaseProps, Shape, Size } from '../types';
export const BTN = 'btn';

export const COLOR_MAP: Record<ComponentColor, string> = {
  primary: 'btn-primary',
  secondary: 'btn-secondary',
  error: 'btn-error',
  info: 'btn-info',
  success: 'btn-success',
  warning: 'btn-warning',
  accent: 'btn-accent',
  neutral: 'btn-neutral',
  ghost: 'btn-ghost',
};

type Variant = 'outline' | 'link' | 'glass';
export const VARIANT_MAP: Record<Variant, string> = {
  outline: 'btn-outline',
  link: 'btn-link',
  glass: 'glass',
};

export const SIZE_MAP: Record<Size, string> = {
  xs: 'btn-xs',
  sm: 'btn-sm',
  md: 'btn-md',
  lg: 'btn-lg',
};

export const SHAPE_MAP: Record<Shape, string> = {
  circle: 'btn-circle',
  square: 'btn-square',
};

export const BUTTON_MAP = {
  ...COLOR_MAP,
  ...VARIANT_MAP,
  ...SIZE_MAP,
  ...SHAPE_MAP,
  active: 'btn-active',
  disabled: 'btn-disabled',
  noAnimation: 'no-animation',
  wide: 'btn-wide',
  fullWidth: 'btn-block',
  responsive: 'btn-xs md:btn-sm lg:btn-md xl:btn-lg',
};

export interface ButtonProps
  extends Omit<ButtonHTMLAttributes<HTMLButtonElement>, 'color'>,
    IComponentBaseProps {
  href?: string;
  shape?: Shape;
  size?: Size;
  variant?: 'outline' | 'link' | 'glass';
  color?: ComponentColor;
  wide?: boolean;
  fullWidth?: boolean;
  responsive?: boolean;
  noAnimation?: boolean;
  loading?: boolean;
  loadingVariant?: LoadingVariant;
  active?: boolean;
  startIcon?: ReactNode;
  endIcon?: ReactNode;
}
