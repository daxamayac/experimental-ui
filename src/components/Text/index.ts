import Text from './Text';

export type { TextProps } from './Text.interface';
export default Text;
