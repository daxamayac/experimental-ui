import { render } from '@testing-library/react';

import Swap from './index';

describe('Swap', () => {
  it('Should render Swap', () => {
    render(<Swap name="swap-name" onElement="On" offElement="Off" />);
  });

  it('Should render onElement and offElement', () => {
    const { getByText } = render(
      <Swap name="swap-name" onElement="On" offElement="Off" />,
    );
    expect(getByText('On')).toBeInTheDocument();
    expect(getByText('Off')).toBeInTheDocument();
  });

  it('Should apply active prop', () => {
    const { container } = render(
      <Swap name="swap-name" onElement="On" offElement="Off" active />,
    );
    expect(container.firstChild).toHaveClass('swap-active');
  });

  it('Should apply rotate prop', () => {
    const { container } = render(
      <Swap name="swap-name" onElement="On" offElement="Off" rotate />,
    );
    expect(container.firstChild).toHaveClass('swap-rotate');
  });

  it('Should apply flip prop', () => {
    const { container } = render(
      <Swap name="swap-name" onElement="On" offElement="Off" flip />,
    );
    expect(container.firstChild).toHaveClass('swap-flip');
  });

  it('Should apply additional class names', () => {
    const { container } = render(
      <Swap
        name="swap-name"
        onElement="On"
        offElement="Off"
        className="text-3xl"
      />,
    );
    expect(container.firstChild).toHaveClass('text-3xl');
  });

  it('Should render onElement and offElement as react fragment', () => {
    const { getByText } = render(
      <Swap name="swap-name" onElement={<>On</>} offElement={<>Off</>} />,
    );
    expect(getByText('On')).toBeInTheDocument();
    expect(getByText('Off')).toBeInTheDocument();
  });

  it('Should render onElement and offElement node', () => {
    const { getByText } = render(
      <Swap
        name="swap-name"
        onElement={<div>On</div>}
        offElement={<div>Off</div>}
      />,
    );
    expect(getByText('On')).toBeInTheDocument();
    expect(getByText('Off')).toBeInTheDocument();
  });
});
