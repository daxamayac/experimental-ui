import { forwardRef, ReactElement, useMemo } from 'react';
import { mergeRefs } from 'react-merge-refs';
import clsx from 'clsx';
import { twMerge } from 'tailwind-merge';
import {
  RefCallBack,
  useFormContext,
  UseFormRegisterReturn,
} from 'react-hook-form';

import { wrapWithElementIfInvalid } from '../utils';

import { SWAP, SwapProps } from './Swap.interface';

const Swap = forwardRef<HTMLLabelElement, SwapProps>(
  (
    {
      onElement,
      offElement,
      active,
      rotate = false,
      name,
      flip = false,
      dataTheme,
      register,
      className,
      disabled,
      ...props
    },
    ref,
  ): ReactElement => {
    const formContext = useFormContext();
    let registerRef: RefCallBack | null = null;
    let registerRest: Omit<UseFormRegisterReturn, 'ref'> | undefined;

    if (formContext) {
      register = formContext.register;
      // TODO posible? error = error ?? (formContext.formState.errors[name] as FieldError);
      disabled = disabled ?? formContext.formState.isSubmitting;
      const { ref, ...rest } = register(name);
      registerRest = rest;
      registerRef = ref;
    }

    const classes = twMerge(
      SWAP,
      'text-center w-fit',
      className,
      clsx({
        'swap-active': active,
        'swap-rotate': rotate,
        'swap-flip': flip,
      }),
    );
    // These next two pieces allow classname to be added to valid elements, or wrap invalid elements with a div and the classname
    const onEl = useMemo(
      () =>
        wrapWithElementIfInvalid({
          node: onElement,
          wrapper: <div></div>,
          props: { className: 'swap-on' },
        }),
      [onElement],
    );

    const offEl = useMemo(
      () =>
        wrapWithElementIfInvalid({
          node: offElement,
          wrapper: <div></div>,
          props: { className: 'swap-off' },
        }),
      [offElement],
    );

    return (
      <label
        {...props}
        data-theme={dataTheme}
        className={classes}
        ref={mergeRefs([registerRef, ref])}
      >
        <input type="checkbox" {...registerRest} disabled={disabled} />
        {onEl}
        {offEl}
      </label>
    );
  },
);

Swap.displayName = 'Swap';
export default Swap;
