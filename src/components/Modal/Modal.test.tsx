// @vitest-environment happy-dom
import { render, screen } from '@testing-library/react';
import { ReactElement, useState } from 'react';

import { Button } from '../index';
import userEvent from '@testing-library/user-event';

import Modal from './index';

function setup(jsx: ReactElement) {
  return {
    user: userEvent.setup(),
    ...render(jsx),
  };
}
const BasicModal = () => {
  const [modalOpen, setModalOpen] = useState(false);
  const handleClose = () => {
    setModalOpen(false);
  };

  const handleAgree = () => {
    console.debug('call service or other action to agree');
    setModalOpen(false);
  };
  return (
    <div className="mt-20 border-2" data-testid="modal-container">
      <Modal onClose={handleClose} open={modalOpen}>
        Modal info
        <Button onClick={handleClose}>Close</Button>
        <Button onClick={handleAgree}>Agree</Button>
      </Modal>
      <Button onClick={() => setModalOpen(true)}>Open Modal</Button>
    </div>
  );
};

describe('Modal', () => {
  it('Should render Modal', () => {
    render(<BasicModal />);
    expect(screen.getByTestId('modal-container').firstChild).toHaveClass(
      'modal',
    );
  });

  it('Should render Modal open', async () => {
    const { user } = setup(<BasicModal />);
    await user.click(screen.getByText('Open Modal'));
    expect(screen.getByTestId('modal-container').firstChild).toHaveClass(
      'modal modal-open',
    );
  });
});
