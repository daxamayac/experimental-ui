import { faWhatsapp } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import React, { useEffect, useState } from 'react';

import Loading from './components/Loading';
import { MarkProps } from './components/Range/Range.CustomMarks';
import { yupResolver } from '@hookform/resolvers/yup';

import {
  Button,
  Checkbox,
  Form,
  Modal,
  Range,
  Swap,
  Text,
  Toggle,
} from './components';

import * as yup from 'yup';

const customMarks: MarkProps[] = [
  {
    value: 0,
    label: <FontAwesomeIcon icon={faWhatsapp} className="h-6" />,
  },
  {
    value: 50,
    label: (
      <>
        50
        <FontAwesomeIcon icon={faWhatsapp} className="h-6" />
      </>
    ),
  },
  {
    value: 20,
    label: (
      <>
        20
        <FontAwesomeIcon icon={faWhatsapp} className="h-6" />
      </>
    ),
  },
  {
    value: 40,
    label: <FontAwesomeIcon icon={faWhatsapp} className="h-6" />,
    showInScreen: 'md',
  },
  {
    value: 100,
    label: (
      <>
        100
        <FontAwesomeIcon icon={faWhatsapp} className="h-6" />
      </>
    ),
  },
  {
    value: 150,
    label: <FontAwesomeIcon icon={faWhatsapp} className="h-6" />,
  },
];

interface FormValues {
  text0: string;
}

const yupSchema: yup.ObjectSchema<FormValues> = yup.object().shape({
  text0: yup.string().required('Este campo es requerido'),
});
export default function App() {
  const [modalOpen, setModalOpen] = useState(false);
  const handleClose = () => {
    setModalOpen(false);
  };

  const handleAgree = () => {
    console.debug('call service or other action to agree');
    setModalOpen(false);
  };

  const onSubmit = async (data: FormValues) => {
    await new Promise((resolve) => {
      setTimeout(() => {
        console.debug(data);
        resolve(undefined);
      }, 200);
    });
  };
  const defaultValues: FormValues = {
    text0: '',
  };
  const ref = React.createRef<HTMLInputElement>();
  useEffect(() => {
    ref.current?.focus();
  }, []);
  return (
    <div className="h-full w-screen bg-blue-50 p-5">
      <Form
        defaultValues={defaultValues}
        resolver={yupResolver(yupSchema)}
        onSubmit={onSubmit}
        className="m-auto flex w-11/12 flex-col gap-5 border-2 p-5"
      >
        <h1 className="text-3xl font-bold text-black">
          Experimental Dax Lib with Form
        </h1>

        <div className="flex flex-col gap-5 rounded-xl border-2 p-2">
          <Text
            name="text0"
            label="Name0"
            placeholder="David Alexander"
            helper="Ingese su nombre de pila"
          />
          <Text
            name="text3"
            label="Name3"
            size="lg"
            placeholder="David Alexander"
            helper="Ingresa al menos un nombre"
          />{' '}
        </div>

        <div className=" flex flex-col gap-5 rounded-xl border-2 p-2">
          <Checkbox
            name="checkbox0"
            size="xs"
            label={<span>custom label react node</span>}
          />

          <Checkbox
            name="checkbox1"
            size="lg"
            label={<span>custom label react node</span>}
          />

          <Checkbox
            name="checkbox2"
            label={<span>custom label react node</span>}
            labelPosition="left"
          />
          <Checkbox
            name="checkbox3"
            label={<span>custom label react node</span>}
          />
        </div>

        <div className=" flex flex-col gap-5 rounded-xl border-2 p-2">
          <Toggle
            name="toggle0"
            size="xs"
            label="Aceptar términos y condiciones."
          />
          <Toggle
            name="toggle1"
            size="lg"
            label="Aceptar términos y condiciones."
          />
          <Toggle
            name="toggle2"
            size="lg"
            ref={ref}
            labelPosition="left"
            label="Aceptar términos y condiciones."
          />
          <Text
            name="text2"
            label="Name"
            size="xs"
            placeholder="David Alexander ref"
            helper="Ingresa al menos un nombre"
          />
          <Toggle
            name="toggle3"
            size="xs"
            labelPosition="left"
            label="Aceptar términos y condiciones."
          />
        </div>

        <div className=" flex flex-col gap-5 rounded-xl border-2 p-2">
          <Swap name="swap" onElement="ON" offElement="OFF" />
          <Swap
            name="swap"
            onElement={<span className="text-green-600">ON</span>}
            offElement={<span className="text-red-600">OFF</span>}
          />
        </div>
        <div className=" flex flex-col gap-5 rounded-xl border-2 p-2">
          <Range name="range" marks={customMarks} step={10} min={0} max={150} />
        </div>
        <Button wide color="primary">
          Boton
        </Button>
      </Form>

      <div className="mt-20 border-2">
        noForm
        <Checkbox
          name="checkbox-fuera0"
          label={<span>custom label react node</span>}
        />
        <Loading size="lg" color="primary" />
      </div>

      <div className="mt-20 border-2">
        noForm
        <Modal onClose={handleClose} open={modalOpen}>
          Modal info
          <Button onClick={handleClose}>Close</Button>
          <Button onClick={handleAgree}>Agree</Button>
        </Modal>
        <Button onClick={() => setModalOpen(true)}>Open Modal</Button>
      </div>
    </div>
  );
}
