import { InputHTMLAttributes, ReactNode } from 'react';
import { FieldError, FieldValues, UseFormRegister } from 'react-hook-form';

import { ComponentColor, IComponentBaseProps, Size } from '../types';

export const TOGGLE = 'toggle';

export const COLOR_MAP = {
  primary: 'toggle-primary',
  secondary: 'toggle-secondary',
  error: 'toggle-error',
  info: 'toggle-info',
  success: 'toggle-success',
  warning: 'toggle-warning',
  accent: 'toggle-accent',
};

export const SIZE_MAP = {
  xs: 'toggle-xs',
  sm: 'toggle-sm',
  md: 'toggle-md',
  lg: 'toggle-lg',
};

export const TOGGLE_MAP = {
  ...SIZE_MAP,
  ...COLOR_MAP,
};

export interface ToggleProps
  extends Omit<InputHTMLAttributes<HTMLInputElement>, 'size'>,
    IComponentBaseProps {
  color?: Exclude<ComponentColor, 'ghost' | 'neutral'>;
  name: string;
  size?: Size;
  className?: string;
  label?: ReactNode;
  labelPosition?: 'left' | 'right';
  register?: UseFormRegister<FieldValues>;
  error?: FieldError;
}
