import { ReactElement } from 'react';
import clsx from 'clsx';
import { twMerge } from 'tailwind-merge';

import { defaultSize } from '../constants';

import { LOADING, LOADING_MAP, LoadingProps } from './Loading.interface';

const Loading = ({
  size = defaultSize,
  variant = 'spinner',
  color,
  dataTheme,
  className,
  style,
  ...props
}: LoadingProps): ReactElement => {
  const classes = twMerge(
    LOADING,
    className,
    clsx(
      color && LOADING_MAP[color],
      size !== 'md' && LOADING_MAP[size],
      variant !== 'spinner' && LOADING_MAP[variant],
    ),
  );
  return (
    <span {...props} data-theme={dataTheme} className={classes} style={style} />
  );
};

export default Loading;
