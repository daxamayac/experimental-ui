import { render } from '@testing-library/react';
import { expect } from 'vitest';

import Label from './';

describe('Label', () => {
  it('Should render label in input', () => {
    const { container } = render(
      <Label label="Given Name" data-testid="label" position="top">
        <input />
      </Label>,
    );

    const formControl = container.querySelector('.form-control');
    expect(formControl).toBeInTheDocument();
    expect(formControl?.firstChild).toHaveClass('label-text');

    const label = container.querySelector('.label-text');
    expect(label).toBeInTheDocument();
    expect(label).toHaveTextContent('Given Name');
  });
  it('Should render label in input', () => {
    const { container } = render(
      <Label
        label={<div>Given Name</div>}
        data-testid="label"
        position="left"
        helper="ayuda"
      >
        <input />
      </Label>,
    );

    const formControl = container.querySelector('.form-control');
    expect(formControl).toBeInTheDocument();
    expect(formControl?.firstChild).toHaveClass('label-text');

    const label = container.querySelector('.label-text');
    expect(label).toBeInTheDocument();
    expect(label).toHaveTextContent('Given Name');
  });
});
