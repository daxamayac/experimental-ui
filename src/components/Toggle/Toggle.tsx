import { forwardRef, ReactElement } from 'react';
import { mergeRefs } from 'react-merge-refs';
import clsx from 'clsx';
import { twMerge } from 'tailwind-merge';
import {
  FieldError,
  RefCallBack,
  useFormContext,
  UseFormRegisterReturn,
} from 'react-hook-form';

import Label from '../Label';

import { TOGGLE, TOGGLE_MAP, ToggleProps } from './Toggle.interface';

const Toggle = forwardRef<HTMLInputElement, ToggleProps>(
  (
    {
      name,
      color,
      label,
      labelPosition = 'right',
      size = 'md',
      className,
      dataTheme,
      register,
      disabled,
      error,
      ...props
    },
    ref,
  ): ReactElement => {
    const formContext = useFormContext();
    let registerRef: RefCallBack;
    let registerRest: Omit<UseFormRegisterReturn, 'ref'> | undefined;

    if (formContext) {
      register = formContext.register;
      error = error ?? (formContext.formState.errors[name] as FieldError);
      disabled = disabled ?? formContext.formState.isSubmitting;
      const { ref, ...rest } = register(name);
      registerRest = rest;
      registerRef = ref;
    }
    const toggleClassName = twMerge(
      TOGGLE,
      clsx(size && TOGGLE_MAP[size], color && TOGGLE_MAP[color], {
        'toggle-error': !!error,
      }),
      className,
    );

    const toggleInput = (
      <input
        type="checkbox"
        ref={(el: HTMLInputElement) => {
          mergeRefs([registerRef, ref])(el);
        }}
        id={name}
        data-theme={dataTheme}
        className={toggleClassName}
        disabled={disabled}
        {...props}
        {...registerRest}
      />
    );

    if (label) {
      return (
        <Label
          htmlFor={name}
          position={labelPosition}
          label={label}
          error={error?.message}
        >
          {toggleInput}
        </Label>
      );
    }

    return toggleInput;
  },
);

Toggle.displayName = 'Toggle';
export default Toggle;
