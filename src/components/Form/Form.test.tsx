import { render, screen } from '@testing-library/react';
import { ReactElement } from 'react';
import { expect } from 'vitest';

import Button from '../Button';
import Checkbox from '../Checkbox';
import Swap from '../Swap';
import Text from '../Text';
import userEvent from '@testing-library/user-event';

import Form from './index';

function setup(jsx: ReactElement) {
  return {
    user: userEvent.setup(),
    ...render(jsx),
  };
}

describe('Text', () => {
  it('Should render Form,Text and Button', () => {
    render(
      <Form onSubmit={(data: { 'text-name': string }) => data}>
        <Text name="text-name" />
        <Button>Submit</Button>
      </Form>,
    );

    expect(screen.getAllByRole('textbox').length).toBe(1);
    expect(screen.getAllByRole('button').length).toBe(1);
  });

  it('Should send data of Text', async () => {
    const mockSubmit = vi.fn();

    const { user } = setup(
      <Form onSubmit={mockSubmit}>
        <Text name="text-name" />
        <Button type="submit">Submit</Button>
      </Form>,
    );

    await user.type(screen.getByRole('textbox'), 'David');
    await user.click(screen.getByRole('button'));

    const data = mockSubmit.mock.calls[0][0];

    expect(mockSubmit).toHaveBeenCalledTimes(1);
    expect(data).toEqual({ 'text-name': 'David' });
  });

  it('Should render Form, Swap and Button', () => {
    render(
      <Form
        onSubmit={(data: { 'swap-name': boolean | null }) =>
          console.debug(data)
        }
      >
        <Swap name="swap-name" onElement="ON" offElement="OFF" />
        <Button>Submit</Button>
      </Form>,
    );

    expect(screen.getAllByRole('checkbox').length).toBe(1);
    expect(screen.getAllByRole('button').length).toBe(1);
  });

  it('Should send data of Swap with true value', async () => {
    const mockSubmit = vi.fn();

    const { user } = setup(
      <Form onSubmit={mockSubmit}>
        <Swap name="swap-name" onElement="ON" offElement="offElement" />
        <Button type="submit">Submit</Button>
      </Form>,
    );

    await user.click(screen.getByRole('checkbox'));
    await user.click(screen.getByRole('button'));

    const data = mockSubmit.mock.calls[0][0];

    expect(mockSubmit).toHaveBeenCalledTimes(1);
    expect(data).toEqual({ 'swap-name': true });
  });

  it('Should send data of Swap with false value', async () => {
    const mockSubmit = vi.fn();

    const { user } = setup(
      <Form onSubmit={mockSubmit}>
        <Swap name="swap-name" onElement="ON" offElement="offElement" />
        <Button type="submit">Submit</Button>
      </Form>,
    );

    await user.dblClick(screen.getByRole('checkbox'));
    await user.click(screen.getByRole('button'));

    const data = mockSubmit.mock.calls[0][0];

    expect(mockSubmit).toHaveBeenCalledTimes(1);
    expect(data).toEqual({ 'swap-name': false });
  });

  it('Should send data of Swap with default false value', async () => {
    const mockSubmit = vi.fn();

    const { user } = setup(
      <Form onSubmit={mockSubmit}>
        <Swap name="swap-name" onElement="ON" offElement="offElement" />
        <Button type="submit">Submit</Button>
      </Form>,
    );

    await user.click(screen.getByRole('button'));

    const data = mockSubmit.mock.calls[0][0];

    expect(mockSubmit).toHaveBeenCalledTimes(1);
    expect(data).toEqual({ 'swap-name': false });
  });

  it('Should render Form, Checkbox and Button', () => {
    render(
      <Form
        onSubmit={(data: { 'checkbox-name': boolean | null }) =>
          console.debug(data)
        }
      >
        <Checkbox name="checkbox-name" />
        <Button>Submit</Button>
      </Form>,
    );

    expect(screen.getAllByRole('checkbox').length).toBe(1);
    expect(screen.getAllByRole('button').length).toBe(1);
  });

  it('Should send data of Checkbox with default null', async () => {
    const mockSubmit = vi.fn();

    const { user } = setup(
      <Form onSubmit={mockSubmit}>
        <Checkbox name="checkbox-name" />
        <Button type="submit">Submit</Button>
      </Form>,
    );

    await user.click(screen.getByRole('button'));

    const data = mockSubmit.mock.calls[0][0];

    expect(mockSubmit).toHaveBeenCalledTimes(1);
    expect(data).toEqual({ 'checkbox-name': null });
  });

  it('Should send data of Checkbox with false from defaultValues', async () => {
    const mockSubmit = vi.fn();

    const { user } = setup(
      <Form onSubmit={mockSubmit} defaultValues={{ 'checkbox-name': false }}>
        <Checkbox name="checkbox-name" />
        <Button type="submit">Submit</Button>
      </Form>,
    );

    await user.click(screen.getByRole('button'));

    const data = mockSubmit.mock.calls[0][0];

    expect(mockSubmit).toHaveBeenCalledTimes(1);
    expect(data).toEqual({ 'checkbox-name': false });
  });
});
