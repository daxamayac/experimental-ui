import fs from 'fs';
const source = fs.readFileSync('./src/index.css', 'utf8');
const lines = source
  .split('\n')
  .filter((line, index) => index !== 0 && index !== 2)
  .join('\n');
fs.writeFileSync('./public/index.css', lines);
