import { useEffect } from 'react';

interface ModalEffectsProps {
  current: HTMLDialogElement | null;
  disableEscapeKeyDown: boolean;
  onClose: () => void;
  open: boolean;
}
export const useModalEffects = ({
  current,
  disableEscapeKeyDown,
  onClose,
  open,
}: ModalEffectsProps): void => {
  // manage escape key down event
  useEffect(() => {
    const cancelHandler = (event: Event) => {
      event.preventDefault();
    };
    if (disableEscapeKeyDown) {
      current?.addEventListener('cancel', cancelHandler);
    }
    return () => {
      current?.removeEventListener('cancel', cancelHandler);
    };
  }, [disableEscapeKeyDown, current, onClose]);

  // manage clos event
  useEffect(() => {
    const closeHandler = () => {
      onClose();
    };

    current?.addEventListener('close', closeHandler);
    return () => {
      current?.removeEventListener('close', closeHandler);
    };
  }, [current, onClose]);

  // manage if prop is open
  useEffect(() => {
    if (open) {
      current?.showModal();
    } else {
      current?.close();
    }
  }, [current, open]);
};
