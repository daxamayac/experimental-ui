import type { Meta, StoryObj } from '@storybook/react';

import { BADGE } from '@geometricpanda/storybook-addon-badges';

import Toggle from '.';
import { v4 as uuid } from 'uuid';

const meta = {
  title: 'Inputs/Toggle',
  component: Toggle,
  tags: ['autodocs'],
  parameters: {
    badges: [BADGE.BETA],
  },
  args: {
    name: uuid(),
  },
} satisfies Meta<typeof Toggle>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Colors: Story = {
  render: (args) => (
    <div>
      <div>
        <Toggle {...args} label="Default" />
      </div>
      <div>
        <Toggle
          {...args}
          color="primary"
          label="Primary"
          placeholder="Primary"
        />
        <Toggle
          {...args}
          color="secondary"
          label="Secondary"
          placeholder="Secondary"
        />
        <Toggle {...args} color="accent" label="Accent" placeholder="Accent" />
      </div>
      <div>
        <Toggle
          {...args}
          color="success"
          label="Success"
          placeholder="Success"
        />
        <Toggle {...args} color="info" label="Info" placeholder="Info" />
        <Toggle
          {...args}
          color="warning"
          label="Warning"
          placeholder="Warning"
        />
        <Toggle {...args} color="error" label="Error" placeholder="Place" />
      </div>
    </div>
  ),
};

export const Size: Story = {
  args: {
    name: 'toggle',
    defaultChecked: true,
  },
  render: (args) => (
    <div>
      <div>
        <Toggle {...args} label="Default" />
      </div>

      <div>
        <Toggle {...args} size="xs" label="Size xs" />
        <Toggle {...args} size="sm" label="Size sm" />
        <Toggle {...args} size="md" label="Size md" />
        <Toggle {...args} size="lg" label="Size lg" />
      </div>
    </div>
  ),
};
