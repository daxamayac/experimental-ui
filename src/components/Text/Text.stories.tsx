import type { Meta, StoryObj } from '@storybook/react';

import { BADGE } from '@geometricpanda/storybook-addon-badges';

import Text from '.';
import { v4 as uuid } from 'uuid';

const meta = {
  title: 'Inputs/Text',
  component: Text,
  tags: ['autodocs'],
  parameters: {
    badges: [BADGE.BETA],
  },
  args: {
    name: uuid(),
  },
} satisfies Meta<typeof Text>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Colors: Story = {
  render: (args) => (
    <div>
      <div className="flex flex-col gap-2 bg-gradient-to-r from-blue-800 to-cyan-500 p-2">
        <Text {...args} placeholder="Default" label="Default" />
        <Text
          {...args}
          label="No border"
          placeholder="No border"
          helper="Helper No border"
        />
        <Text {...args} label="Ghost" color="ghost" helper="Helper ghost" />
      </div>
      <div className="flex flex-col gap-2 p-2">
        <Text {...args} placeholder="Default" label="Default" />
        <Text
          {...args}
          color="primary"
          label="Primary"
          placeholder="Primary"
          helper="Helper primary"
        />
        <Text
          {...args}
          color="secondary"
          label="Secondary"
          placeholder="Secondary"
        />
        <Text {...args} color="accent" label="Accent" placeholder="Accent" />
        <Text {...args} color="ghost" label="Ghost" placeholder="Ghost" />
      </div>
      <div className="flex flex-col gap-2 p-2">
        <Text {...args} color="success" placeholder="Success" />
        <Text {...args} color="info" placeholder="Info" />
        <Text {...args} color="warning" placeholder="Warning" />
        <Text
          {...args}
          color="error"
          label="Error"
          helper="Helper"
          error={{ message: 'Error description', type: 'manual' }}
          placeholder="Place"
        />
      </div>
    </div>
  ),
};

export const Size: Story = {
  args: {
    color: 'primary',
  },
  render: (args) => (
    <div className="flex flex-col gap-2 p-2">
      <div className="flex flex-col gap-2 p-2">
        <Text {...args} label="Default" />
      </div>

      <div className="flex flex-col gap-2 p-2">
        <Text {...args} size="xs" label="Size xs" />
        <Text {...args} size="sm" label="Size sm" />
        <Text {...args} size="md" label="Size md" />
        <Text {...args} size="lg" label="Size lg" />
      </div>
    </div>
  ),
};
