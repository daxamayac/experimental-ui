import { InputHTMLAttributes, ReactNode } from 'react';
import { FieldError, FieldValues, UseFormRegister } from 'react-hook-form';

import { ComponentColor, IComponentBaseProps, Size } from '../types';

export const CHECKBOX = 'checkbox';

export const SIZE_MAP = {
  xs: 'checkbox-xs',
  sm: 'checkbox-sm',
  md: 'checkbox-md',
  lg: 'checkbox-lg',
};

export const COLOR_MAP = {
  primary: 'checkbox-primary',
  secondary: 'checkbox-secondary',
  error: 'checkbox-error',
  info: 'checkbox-info',
  success: 'checkbox-success',
  warning: 'checkbox-warning',
  accent: 'checkbox-accent',
};

export const CHECKBOX_MAP = {
  ...SIZE_MAP,
  ...COLOR_MAP,
};
export interface CheckboxProps
  extends Omit<
      InputHTMLAttributes<HTMLInputElement>,
      'size' | 'placeholder' | 'label'
    >,
    IComponentBaseProps {
  color?: Exclude<ComponentColor, 'ghost' | 'neutral'>;
  indeterminate?: boolean;
  name: string;
  size?: Size;
  label?: ReactNode;
  error?: FieldError;
  labelPosition?: 'left' | 'right';
  register?: UseFormRegister<FieldValues>;
}
