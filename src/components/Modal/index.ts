import Modal from './Modal';

export type { ModalProps } from './Modal.interface';
export default Modal;
