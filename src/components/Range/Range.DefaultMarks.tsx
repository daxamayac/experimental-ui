import { memo, useMemo } from 'react';
import clsx from 'clsx';
import { twMerge } from 'tailwind-merge';

function DefaultMarks({
  step,
  min,
  max,
  className,
  classes,
}: {
  step: number;
  min: number;
  max: number;
  className?: string;
  classes?: { mark?: string };
}) {
  const diff = useMemo(() => max - min, [min, max]);
  if (diff % step !== 0)
    throw new Error('step no es múltiplo de la diferencia entre min y max');

  const numSteps = useMemo(() => {
    return Math.floor((max - min) / step);
  }, [min, max, step]);

  const leftValues = useMemo(() => {
    const values = [];
    for (let i = 0; i <= numSteps; i++) {
      const left = `${(i * 100) / numSteps}%`;
      values.push(left);
    }
    return values;
  }, [numSteps]);

  return (
    <div className={clsx(' w-full', className)}>
      <div className="relative flex w-full">
        {leftValues.map((left, i) => (
          <div
            className={twMerge(
              'absolute -translate-x-1/2 cursor-pointer text-center  first:relative',
              classes?.mark,
            )}
            style={{ left }}
            key={left}
          >
            <>
              <div>|</div>
              <div>{i * step + min}</div>
            </>
          </div>
        ))}
      </div>
    </div>
  );
}
export default memo(DefaultMarks);
