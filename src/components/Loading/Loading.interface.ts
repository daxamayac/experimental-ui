import { HTMLAttributes } from 'react';

import { ComponentColor, IComponentBaseProps, Size } from '../types';

export const LOADING = 'loading';
type LoadingColor = Exclude<ComponentColor, 'ghost'>;

export const COLOR_MAP: Record<LoadingColor, string> = {
  primary: 'text-primary',
  secondary: 'text-secondary',
  accent: 'text-accent',
  info: 'text-info',
  success: 'text-success',
  warning: 'text-warning',
  error: 'text-error',
  neutral: 'text-neutral',
};

export const SIZE_MAP: Record<Size, string> = {
  xs: 'loading-xs',
  sm: 'loading-sm',
  md: 'loading-md',
  lg: 'loading-lg',
};

export type Variant =
  | 'spinner'
  | 'dots'
  | 'ring'
  | 'ball'
  | 'bars'
  | 'infinity';

export const VARIANT_MAP: Record<Variant, string> = {
  spinner: 'loading-spinner',
  dots: 'loading-dots',
  ring: 'loading-ring',
  ball: 'loading-ball',
  bars: 'loading-bars',
  infinity: 'loading-infinity',
};

export const LOADING_MAP = {
  ...COLOR_MAP,
  ...SIZE_MAP,
  ...VARIANT_MAP,
};

export interface LoadingProps
  extends HTMLAttributes<HTMLSpanElement>,
    IComponentBaseProps {
  size?: Size;
  color?: Exclude<ComponentColor, 'ghost'>;
  variant?: Variant;
}
