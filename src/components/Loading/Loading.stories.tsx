import type { Meta, StoryObj } from '@storybook/react';

import { BADGE } from '@geometricpanda/storybook-addon-badges';

import Loading from '.';
const meta = {
  title: 'Data Display/Loading',
  component: Loading,
  tags: ['autodocs'],
  parameters: {
    badges: [BADGE.EXPERIMENTAL],
  },
} satisfies Meta<typeof Loading>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Colors: Story = {
  render: (args) => (
    <div className="flex flex-col gap-2">
      <div>
        <Loading {...args}>Default</Loading>
      </div>
      <div className="flex gap-2">
        <Loading {...args} color="primary" />
        <Loading {...args} color="secondary" />

        <Loading {...args} color="accent" />
        <Loading {...args} />
      </div>
      <div className="flex gap-2">
        <Loading {...args} color="success" />
        <Loading {...args} color="info" />
        <Loading {...args} color="warning" />
        <Loading {...args} color="error" />
      </div>
    </div>
  ),
};

export const Variants: Story = {
  render: (args) => (
    <div className="flex flex-col gap-2">
      <div>
        <Loading {...args}>Default</Loading>
      </div>

      <div className="flex gap-5">
        <Loading {...args} variant="ball" />
        <Loading {...args} variant="bars" />
        <Loading {...args} variant="ring" />
        <Loading {...args} variant="dots" />
        <Loading {...args} variant="infinity" />
      </div>
    </div>
  ),
};
export const Sizes: Story = {
  render: (args) => (
    <div className="flex flex-col  p-5">
      <div className="flex gap-5">
        <Loading {...args} size="xs" />
        <Loading {...args} size="sm" />
        <Loading {...args} />
        <Loading {...args} size="lg" />
      </div>
      <div className="flex gap-5">
        <Loading {...args} size="xs" variant="ball" />
        <Loading {...args} size="sm" variant="ball" />
        <Loading {...args} variant="ball" />
        <Loading {...args} size="lg" variant="ball" />
      </div>
      <div className="flex gap-5">
        <Loading {...args} size="xs" variant="bars" />
        <Loading {...args} size="sm" variant="bars" />
        <Loading {...args} variant="bars" />
        <Loading {...args} size="lg" variant="bars" />
      </div>
      <div className="flex gap-5">
        <Loading {...args} size="xs" variant="ring" />
        <Loading {...args} size="sm" variant="ring" />
        <Loading {...args} variant="ring" />
        <Loading {...args} size="lg" variant="ring" />
      </div>
      <div className="flex gap-5">
        <Loading {...args} size="xs" variant="dots" />
        <Loading {...args} size="sm" variant="dots" />
        <Loading {...args} variant="dots" />
        <Loading {...args} size="lg" variant="dots" />
      </div>
      <div className="flex gap-5">
        <Loading {...args} size="xs" variant="infinity" />
        <Loading {...args} size="sm" variant="infinity" />
        <Loading {...args} variant="infinity" />
        <Loading {...args} size="lg" variant="infinity" />
      </div>
    </div>
  ),
};
