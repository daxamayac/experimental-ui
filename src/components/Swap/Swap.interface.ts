import { LabelHTMLAttributes, ReactNode } from 'react';
import { FieldValues, UseFormRegister } from 'react-hook-form';

import { IComponentBaseProps } from '../types';

export const SWAP = 'swap';

export type SwapProps = LabelHTMLAttributes<HTMLLabelElement> &
  IComponentBaseProps & {
    onElement: ReactNode | ReactNode[];
    offElement: ReactNode | ReactNode[];
    active?: boolean;
    rotate?: boolean;
    flip?: boolean;
    register?: UseFormRegister<FieldValues>;
    name: string;
    disabled?: boolean;
    errors?: {
      message?: string;
    };
  };
