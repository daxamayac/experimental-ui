import type { Meta, StoryObj } from '@storybook/react';

import { BADGE } from '@geometricpanda/storybook-addon-badges';

import Swap from '.';
import { v4 as uuid } from 'uuid';

const meta = {
  title: 'Inputs/Swap',
  component: Swap,
  tags: ['autodocs'],
  parameters: {
    badges: [BADGE.BETA],
  },
  args: {
    name: uuid(),
    onElement: 'On',
    offElement: 'Off',
  },
} satisfies Meta<typeof Swap>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Colors: Story = {
  render: (args) => (
    <div>
      <div>
        <Swap {...args} />
      </div>
      <div className="flex gap-5">
        <Swap {...args} color="primary" />
        <Swap {...args} color="secondary" />
        <Swap {...args} color="accent" />
      </div>
      <div className="flex gap-5">
        <Swap {...args} color="success" />
        <Swap {...args} color="info" />
        <Swap {...args} color="warning" />
        <Swap {...args} color="error" />
      </div>
    </div>
  ),
};
