import Button from './Button';

export type { ButtonProps } from './Button.interface';
export default Button;
