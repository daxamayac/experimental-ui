import Form from './Form';

export type { FormProps } from './Form.interface';
export default Form;
