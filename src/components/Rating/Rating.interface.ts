import { HTMLAttributes } from 'react';

import { IComponentBaseProps } from '../types';

export const RATING = 'rating';

export const SIZE_MAP = {
  xs: 'rating-xs',
  sm: 'rating-sm',
  md: 'rating-md',
  lg: 'rating-lg',
};

export interface RatingProps
  extends Omit<HTMLAttributes<HTMLDivElement>, 'onChange'>,
    IComponentBaseProps {
  name: string;
}
