import Label from './Label';

export type { LabelProps } from './Label.interface';
export default Label;
